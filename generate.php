#!/usr/bin/env php
<?php

namespace lucideer;
use lucideer\Site;

require_once 'site.php';

$site_config = json_decode(file_get_contents(dirname(__FILE__) . '/config.json'));

$content_dir = dirname(__FILE__) . '/' . $site_config->site_dir . '/content';

$templates = array_merge(
    glob($content_dir . '/*.mustache'),
    glob($content_dir . '/*/*.mustache'),
    glob($content_dir . '/*/*/*.mustache')
);

$output_dir = realpath(dirname(__FILE__) . '/' . $site_config->output_dir);
$site = new Site($site_config);
foreach ($templates as $template) {
     
    $template = preg_replace('/\.mustache$/', '', $template, 1, $count);
    if ($count !== 1) {
        continue;
    }

    $info = pathinfo($template);
    $info['urlpath'] = str_replace($content_dir, '', $info['dirname']) . '/';
    $info['result'] = $site->renderContent($info['urlpath'] . $info['basename'], ['planet' => 'world']); 

    $dir = $output_dir . $info['urlpath'];
    if (!file_exists($dir)) {
        mkdir($dir, 0755, true);
    }
    file_put_contents($dir . '/' . $info['basename'], $info['result']);
}

