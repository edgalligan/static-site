<?php

namespace lucideer;

require_once 'vendor/mustache/mustache/src/Mustache/Autoloader.php';
\Mustache_Autoloader::register();

class Site extends \Mustache_Engine {

    protected $_layout_template;
    protected $_site_config = array();
    protected $_meta_values = array();

    public function __construct(
        $site_config = array(),
        \Mustache_Logger $logger = null
    ) {
        $logger = $logger? $logger : new \Mustache_Logger_StreamLogger('php://stderr');

        $this->_site_config = (array) $site_config;
        $cfg = (object) $site_config;

        $site_dir = dirname(__FILE__) . '/' . (isset($cfg->site_dir)? $cfg->site_dir : 'site');

        $loader = new \Mustache_Loader_FilesystemLoader($site_dir);
        $partials_loader = new \Mustache_Loader_FilesystemLoader($site_dir . '/partials');

        $engine = parent::__construct([
            'charset' => 'UTF-8',
            'strict_callables' => true,

            'loader' => $loader,
            'partials_loader' => $partials_loader,
            'logger' => $logger,
            'helpers' => $this->defineHelpers(),

//            'cache' => $site_dir . '/cache',
//            'cache_lambda_templates' => true,
        ]);

        $this->_layout_template = $this->loadTemplate($cfg->layout_template);

        return $engine;
    }

    public function renderContent($content_template, $context = array()) {
        $this->_meta_values = array();
        $context = array_merge($this->_site_config, $context);
        $context['content'] = $this->render('content/' . $content_template, $context);

        $context = $this->mergeMeta($context, $this->_meta_values);
        return $this->_layout_template->render($context);
    }

    protected function mergeMeta($old, $new) {
        foreach ($new as $k => $v) {
            if (isset($old[$k]) && is_array($old[$k])) {
                $old[$k] = array_merge($v, $old[$k]);
            }
            else {
                $old[$k] = $v;
            }
        }
        return $old;
    }

    protected function defineHelpers() {
        return [
            'meta' => function($str) {
                $meta_values = json_decode(trim($str));
                $this->_meta_values = $this->mergeMeta($this->_meta_values, $meta_values);
            },
        ];
    }
}

